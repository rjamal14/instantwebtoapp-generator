import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Platform, Alert, Animated, BackHandler, Dimensions, Image, ImageBackground, RefreshControl, ScrollView, Text, View, PushNotificationIOS, DeviceEventEmitter, StatusBar, Button, TouchableHighlight, Touchable, TouchableNativeFeedbackBase, TouchableOpacity, Switch, Linking, useWindowDimensions } from 'react-native';
import { WebView } from 'react-native-webview';
// import GetAppName from 'react-native-get-app-name';
import Config from './Config';
import { SafeAreaView } from 'react-native-safe-area-context';
import AutoHeightImage from 'react-native-auto-height-image';
import messaging from '@react-native-firebase/messaging';
import PushNotification, { Importance } from 'react-native-push-notification';
import CheckBox from 'react-native-check-box';
import AsyncStorage from '@react-native-async-storage/async-storage';

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const INJECTED_JS = `
  window.onscroll = function() {
    window.ReactNativeWebView.postMessage(
      JSON.stringify({
        scrollTop: document.documentElement.scrollTop || document.body.scrollTop
      }),     
    )
  }
`

const SCROLLVIEW_CONTAINER = {
  flex: 1,
  height: "100%",
}

const WEBVIEW = (height) => ({
  width: "100%",
  height,
})

function SplashScreen({ navigation }) {
  const [appName, setAppName] = useState('')
  const dimensions = Dimensions.get('window');
  const imageWidth = dimensions.width;
  const imageHeight = dimensions.height; 
  const fadeAnim = useRef(new Animated.Value(0)).current
 
  useEffect(() => {
    // GetAppName.getAppName((_appName) => {
    //   setAppName(_appName)
    // }) 

    setTimeout(async () => {
      const skipOnBoarding = await AsyncStorage.getItem('skip_onboarding') 
      const permissionRequested = await AsyncStorage.getItem('permission_requested')
      let page = ''

      if (permissionRequested !== null && Platform.OS == 'ios') {
        const authorizationStatus = await messaging().requestPermission({
          providesAppNotificationSettings: true
        })

        if (authorizationStatus === messaging.AuthorizationStatus.AUTHORIZED) {
          console.log('User has notification permissions enabled.')
          page = 'Home'
        } else if (authorizationStatus === messaging.AuthorizationStatus.PROVISIONAL) {
          console.log('User has provisional notification permissions.')
        } else {
          console.log('User has notification permissions disabled')
          page = 'Permission'
        }

        navigation.reset({
          index: 0,
          routes: [{ name: page }],
        });
      } else {
        navigation.reset({
          index: 0,
          routes: [{ name: skipOnBoarding == 'true' ? 'Home' : 'OnBoarding' }],
        });
      }
    }, (Config.SPLASH_SCREEN_TIME * 1000) + 1000)
  }, [])

  return (
    <SafeAreaView style={{ flex: 1 }}>
      {Platform.OS == 'ios' && <StatusBar
        barStyle={'dark-content'}
      />}

      <FadeInView style={{flex: 1}}>
        <View style={{ flex: 1, backgroundColor: Config.BG_COLOR }}>
          {Config.IMG_SPLASH != "" && <View style={{ flex: 1, justifyContent: Config.BG_SPLASH_SNAP }}>
            {/* bg splash screen full width */}
            {Config.BG_SPLASH_POSITION == 'full_width' && <AutoHeightImage
              source={Config.IMG_SPLASH}
              width={imageWidth} />}

            {/* bg splash screen full height */}
            {Config.BG_SPLASH_POSITION == 'full_height' && <Image
              source={Config.IMG_SPLASH}
              style={{ height: '100%', alignSelf: Config.BG_SPLASH_SNAP }} />}

            {Config.BG_SPLASH_POSITION == 'cover' && <Image
              source={Config.IMG_SPLASH}
              style={{ height: '100%', alignSelf: 'center', width: '100%' }} />}
          </View>}
        </View>
      </FadeInView>
    </SafeAreaView>
  );
} 

function CustomButton({ style, label, bgColor, _borderRadius, textColor, onClick }) {
  return(
    <TouchableOpacity onPress={onClick}>
      <View style={{ ...style, flexDirection: 'row', alignItems: 'center', backgroundColor: bgColor, borderRadius: _borderRadius, height: 40, paddingLeft: 32, paddingRight: 32 }}>
        <Text style={{ fontWeight: 'bold', fontSize: 16, color: textColor, alignSelf: 'center' }}>{label}</Text>
      </View>
    </TouchableOpacity>
  )
}

function OnBoardingScreen({ navigation }) {
  const [dontShowAgain, setDontShowAgain] = useState(false)
  const [webViewHeight, setWebViewHeight] = useState(30)
  const { width } = useWindowDimensions();
  const dimensions = Dimensions.get('window');
  const imageWidth = dimensions.width;

  const next = async () => {
    try {
      await AsyncStorage.setItem('skip_onboarding', String(dontShowAgain))
      navigation.navigate(Platform.OS == 'ios' ? 'Permission' : 'Home')
    } catch (e) {
      console.log(e)
    }
  }

  const onWebViewMessage = (e) => {
    try {
      console.log(e.nativeEvent.data)
      setWebViewHeight(parseInt(e.nativeEvent.data))
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      {Platform.OS == 'ios' && <StatusBar
        barStyle={'dark-content'}
      />}
      <View style={{ flex: 1}}>
        <View style={{ width: '100%', height: '100%', position: 'absolute', backgroundColor: Config.onBoarding.bgColorOnBoarding}}>
          {Config.onBoarding.bgImage != "" && <View style={{ flex: 1, justifyContent: Config.onBoarding.bgSnap }}>
            {/* bg splash screen full width */}
            {Config.onBoarding.bgPosition == 'full_width' && <AutoHeightImage
              source={Config.onBoarding.bgImage}
              width={imageWidth} />}

            {/* bg splash screen full height */}
            {Config.onBoarding.bgPosition == 'full_height' && <Image
              source={Config.onBoarding.bgImage}
              style={{ height: '100%', alignSelf: Config.onBoarding.bgSnap }} />}

            {Config.onBoarding.bgPosition == 'cover' && <Image
              source={Config.onBoarding.bgImage}
              style={{ height: '100%', alignSelf: 'center', width: '100%' }} />}
          </View>}
        </View>
        <View style={{ flex: 0.6 }}></View>
        <View style={{ flex: 0.4 }}>
          <View style={{ flex: 1, flexDirection: 'column-reverse' }}>
            <View style={{ padding: 32, flexDirection: 'column', backgroundColor: Config.onBoarding.description == ''  ? '' : '#ffffff', borderTopLeftRadius: 30, borderTopRightRadius: 30 }}>
              <ScrollView>
                {Config.onBoarding.description != '' && <WebView
                  style={{ height: webViewHeight }}
                  setBuiltInZoomControls={false}
                  source={{ html: `<html><meta name="viewport" content="width=device-width, initial-scale=1"><body>${Config.onBoarding.description}</body></html>` }}
                  onMessage={onWebViewMessage}
                  injectedJavaScript='window.ReactNativeWebView.postMessage(document.body.scrollHeight)'
                />}
              </ScrollView>
              {/* <ScrollView>
                <Text style={{ color: '#333333', fontSize: 16 }}>{Config.onBoarding.description}</Text>
              </ScrollView> */}
              <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>  
                <CheckBox
                  style={{ marginRight: 10 }}
                  unCheckedImage={<Image source={require('./checkbox_checked.png')} style={{  width: 26, height: 26 }}/>}
                  checkedImage={<Image source={require('./checkbox_unchecked.png')} style={{  width: 26, height: 26 }}/>}
                  onClick={() => {
                    setDontShowAgain(!dontShowAgain)
                  }}
                  isChecked={dontShowAgain}
                />
                {/* <CheckBox
                  boxType={'square'}
                  disabled={false}
                  lineWidth={1}
                  tintColors={{ true: '#333', false: '#333' }} 
                  animationDuration={0}
                  value={dontShowAgain}
                  onValueChange={(val) => setDontShowAgain(val)}
                />   */}
                <Text style={{ color: Config.onBoarding.checkTextColor, fontWeight: 'bold', fontSize: 13 }}>Don't show this message again</Text>
              </View>

              <CustomButton
                style={{ alignSelf: 'center', marginTop: 10 }}
                bgColor={Config.onBoarding.button.bgColor}
                textColor={Config.onBoarding.button.textColor}
                _borderRadius={Config.onBoarding.button.borderRadius}
                label={Config.onBoarding.button.text}
                onClick={() => next()}
              />
            </View>
          </View> 
        </View>
      </View>
    </SafeAreaView>
  )
}

function PermissionScreen({ navigation }) { 
  const [isEnabled, setIsEnabled] = useState(false); 
  const [permisisonAllowed, setPermisisonAllowed] = useState(false); 
  const lightColor = '#fefefe'
  const darkColor = '#333333'
  const textStyle = { color: Config.permissionTheme == 'light' ? darkColor : lightColor }
 
  async function requestUserPermission() {
    const authorizationStatus = await messaging().requestPermission({ 
      providesAppNotificationSettings: true
    })

    await AsyncStorage.setItem('permission_requested', 'true')

    if (authorizationStatus === messaging.AuthorizationStatus.AUTHORIZED) {
      console.log('User has notification permissions enabled.')
      setPermisisonAllowed(true)
    } else if (authorizationStatus === messaging.AuthorizationStatus.PROVISIONAL) {
      console.log('User has provisional notification permissions.')
    } else {
      console.log('User has notification permissions disabled')
      setIsEnabled(false)
      goToSetting()
    }
  }

  function goToSetting() {
    // GetAppName.getAppName((_appName) => {
      Alert.alert(
        "",
        `Couldn't access Notification. Please go to iOS Settings >  Notifications, allow to send notifications`,
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          {
            text: "Go to setting", onPress: () => Linking.openURL('app-settings://') }
        ]
      );
    // }) 
  }

  function next() {
    navigation.reset({
      index: 0,
      routes: [{ name: 'Home' }],
    });
  }
  
  return (
    // <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, paddingTop: 64, paddingLeft: 32, paddingRight: 32, paddingBottom: 32, backgroundColor: Config.permissionTheme == 'light' ? lightColor : darkColor }}>
        <View style={{ flex: 1 }}>
          <Text style={{ ...textStyle, fontSize: 24, fontWeight: 'bold', letterSpacing: 1, marginBottom: 16, textAlign: 'center' }}>Permission</Text>
          <Text style={textStyle}>To improve app experience, we are requesting you to enable permission below:</Text>
          <View style={{ flexDirection: 'row', marginTop: 32 }}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={{ ...textStyle, fontSize: 16 }}>Push notification</Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
              <Switch
                onValueChange={() => {
                  setIsEnabled(previousState => !previousState)
                  requestUserPermission()
                }}
                value={isEnabled}
              />
            </View>
          </View>
        </View>

        {permisisonAllowed == true && <CustomButton
          style={{ alignSelf: 'center', marginTop: 30 }}
          bgColor={Config.onBoarding.button.bgColor}
          textColor={Config.onBoarding.button.textColor}
          _borderRadius={Config.onBoarding.button.borderRadius}
          label={Config.onBoarding.button.text}
          onClick={() => next()}
        />}
      </View>
    // </SafeAreaView>
  )
}

const FadeInView = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current  

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: (Config.SPLASH_SCREEN_TIME * 1000),
        useNativeDriver: true
      }
    ).start();
  }, [fadeAnim])

  return (
    <Animated.View                 
      style={{
        ...props.style,
        opacity: fadeAnim,         
      }}
    >
      {props.children}
    </Animated.View>
  );
}

function HomeScreen() {
  const webViewRef = useRef(null)
  const [isPullToRefreshEnabled, setPullToRefreshEnabled] = useState(true)
  const [scrollViewHeight, setScrollViewHeight] = useState(0)
  const [canGoBack, setCanGoBack] = useState(false)
  const [refreshing, setRefreshing] = useState(false);
  const [permissions, setPermissions] = useState({});

  const onRefresh = useCallback(() => {
    webViewRef.current.reload()
  }, []);

  const onWebViewMessage = e => {
    const { data } = e.nativeEvent

    try {
      const { scrollTop } = JSON.parse(data)
      setPullToRefreshEnabled(scrollTop === 0)
    } catch (error) { 
      console.log(error);
    }
  }

  useEffect(() => {
    if (Config.IS_USE_FCM) {
      // if (Platform.OS === 'ios') {
      //   requestUserPermission()
      // }

      console.log('listen fcm..')

      messaging()
        .subscribeToTopic(Platform.OS)
        .then(() => console.log(`Subscribed ${Platform.OS} topic`));

      messaging()
        .subscribeToTopic('all')
        .then(() => console.log('Subscribed all topic'));

      const unsubscribe = messaging().onMessage(async remoteMessage => {
        console.log('A new FCM message arrived!', JSON.stringify(remoteMessage))

        if (Platform.OS === 'android') {
          PushNotification.createChannel(
            {
              channelId: "appsinstant-broadcast",
              channelName: "broadcast",
              playSound: true,
              soundName: "default",
              importance: Importance.HIGH,
              vibrate: true,
            },
            (created) => console.log(`createChannel returned '${created}'`)
          );

          PushNotification.localNotification({
            channelId: "appsinstant-broadcast",
            title: remoteMessage.notification.title,
            message: remoteMessage.notification.body,
            bigPictureUrl: remoteMessage.notification.android.imageUrl
          });
        } else {
          PushNotification.localNotification({
            title: remoteMessage.notification.title,
            message: remoteMessage.notification.body,
          });
        }
      });

      return unsubscribe;
    }
  }, []);

  useEffect(() => {
    const backAction = () => {
      if (canGoBack) {
        webViewRef.current.goBack()
      } else {
        BackHandler.exitApp()
      }
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, [canGoBack]);

  const backward = () => {
    if (canGoBack) {
      webViewRef.current.goBack()
    } else {
      BackHandler.exitApp()
    }
  }

  const forward = () => {
    webViewRef.current.goForward()
  }

  return ( 
    <SafeAreaView style={{ flex: 1 }}>
      {Platform.OS == 'ios' && <StatusBar
        barStyle={'dark-content'}
      />}

      <ScrollView
        style={SCROLLVIEW_CONTAINER}
        onLayout={e => setScrollViewHeight(e.nativeEvent.layout.height)}
        contentContainerStyle={{ flex: 1 }}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            enabled={isPullToRefreshEnabled}
            onRefresh={onRefresh}
          />
        }
      >
        <WebView
          allowsBackForwardNavigationGestures={true}
          automaticallyAdjustContentInsets={false}
          startInLoadingState={true}
          ref={webViewRef}
          source={{ uri: Config.WEB_URL }}
          onNavigationStateChange={(data) => setCanGoBack(data.canGoBack)}
          style={WEBVIEW(scrollViewHeight)}
          onMessage={onWebViewMessage}
          injectedJavaScript={INJECTED_JS}
        />
      </ScrollView>
    </SafeAreaView>
  );
}

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen name="OnBoarding" component={OnBoardingScreen} options={{
          headerShown: false,
          orientation: Config.SPLASH_SCREEN_ORIENTATION
        }} />
        <Stack.Screen name="Permission" component={PermissionScreen} options={{
          headerShown: false,
          orientation: Config.SPLASH_SCREEN_ORIENTATION
        }} />
        <Stack.Screen name="Splash" component={SplashScreen} options={{
          headerShown: false,
          orientation: Config.SPLASH_SCREEN_ORIENTATION
        }} />
        <Stack.Screen name="Home" component={HomeScreen} options={{
          headerShown: false,
          orientation: Config.HOME_ORIENTATION
        }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;