export default {
  LOGO: require('./icon.png'),
  LOGO_SPLASH: '',
  IMG_SPLASH: require('./splash_bg.png'),
  WEB_URL: 'https://m.tiktok.com',
  BG_COLOR: '#121111',
  LOGO_SPLASH_POSITION: 'center',
  BG_SPLASH_SNAP: 'center',
  BG_SPLASH_POSITION: 'full_height',
  SPLASH_SCREEN_TIME: 2,
  SPLASH_SCREEN_ORIENTATION: 'portrait',
  HOME_ORIENTATION: 'portrait',
  IS_USE_FCM: false,
  onBoarding: {
    bgImage: '',
    bgSnap: 'center',
    bgPosition: 'full_width',
    checkTextColor: '#ffffff',
    bgColorOnBoarding: '#333333',
    button: {
      bgColor: '#35a0f9',
      textColor: '#FFFFFF',
      text: 'Next',
      borderRadius: 25
    },
    description: ``
  },
  permissionTheme: 'dark'
}