/*
command: 
node build.js --fcm false --typeBuild "ipa" --appName "Appsinstant" --versionCode "1" --versionName "1.0" --applicationId com.appsinstant --webUrl https://appsinstant.com --icon "icon.png" --bgImageSplashIcon "" --bgImageSplash "splash_bg.jpg" --bgImageSplashPosition "auto_height" --bgImageSplashSnap "center" --bgColorSplash "#ffffff" --logoSplashPosition "center" --splashScreenTime "2000" --splashScreenOrientation "lock_vertical" --homeOrientation "lock_vertical" --keystore ""
*/

const util = require('util');
const fs = require('fs')
const https = require('https');
const sharp = require('sharp');
const { exec } = require("child_process");
const execSync = util.promisify(require('child_process').exec);
const { mainModule } = require('process');
const CONFIG = require('minimist')(process.argv.slice(2), {
  string: 'versionName',
  boolean: 'fcm'
}) 
const md5 = require('md5')
const parser = require('xml2json');
const sudo = require('sudo-js');
sudo.setPassword('123apps');

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  global.localStorage = new LocalStorage('./scratch');
}

const TIME_OUT = 30 * 60000 // 10 minutes
// const pathImage = '~/Documents/instantwebtoapp/public/images/'
const pathImage = '/home/deploy/instantwebtoapp/public/images/'
const iosPath = `${__dirname}/ios`

const screenOrientations = {
  'autorotate': 'all',
  'lock_vertical': 'portrait',
  'lock_horizontal': 'landscape'
}

const logoSplashPositions = {
  'top': 'flex-start',
  'center': 'center',
  'bottom': 'flex-end'
}

const bgImageSplashSnapPositions = {
  'top': 'flex-start',
  'left': 'flex-start',
  'center': 'center',
  'right': 'flex-end',
  'bottom': 'flex-end',
}

const bgImageSplashPositions = {
  'auto_width': 'full_height',
  'auto_height': 'full_width',
}

const isAndroid = CONFIG.typeBuild != 'ipa'

function initConfig() {
  const fileConfig = `export default {
  LOGO: require('./${CONFIG.icon}'),
  LOGO_SPLASH: ${CONFIG.bgImageSplashIcon == '' ? `''` : `require('./${CONFIG.bgImageSplashIcon}')`},
  IMG_SPLASH: ${CONFIG.bgImageSplash == '' ? `''` : `require('./${CONFIG.bgImageSplash}')`},
  WEB_URL: '${CONFIG.webUrl}',
  BG_COLOR: '${CONFIG.bgColorSplash}',
  LOGO_SPLASH_POSITION: '${logoSplashPositions[CONFIG.logoSplashPosition]}',
  BG_SPLASH_SNAP: '${bgImageSplashSnapPositions[CONFIG.bgImageSplashSnap]}',
  BG_SPLASH_POSITION: '${bgImageSplashPositions[CONFIG.bgImageSplashPosition]}',
  SPLASH_SCREEN_TIME: ${CONFIG.splashScreenTime},
  SPLASH_SCREEN_ORIENTATION: '${screenOrientations[CONFIG.splashScreenOrientation]}',
  HOME_ORIENTATION: '${screenOrientations[CONFIG.homeOrientation]}',
  IS_USE_FCM: ${CONFIG.fcm},
  onBoarding: {
    bgImage: ${CONFIG.bgImageOnBoarding == '' ? `''` : `require('./${CONFIG.bgImageOnBoarding}')`},
    bgSnap: '${bgImageSplashSnapPositions[CONFIG.bgSnapOnBoarding]}',
    bgPosition: '${bgImageSplashPositions[CONFIG.bgOnBoardingPosition]}',
    checkTextColor: '${CONFIG.checkTxtColor}',
    bgColorOnBoarding: ${(CONFIG.bgColorOnBoarding == undefined || CONFIG.bgColorOnBoarding == null || CONFIG.bgColorOnBoarding == '') ? `'#FFFFFF'` : `'${CONFIG.bgColorOnBoarding}'`},
    button: {
      bgColor: '${CONFIG.btnColor}',
      textColor: '${CONFIG.btnTxtColor}',
      text: '${CONFIG.btnTxt}',
      borderRadius: ${CONFIG.btnRadius}
    },
    description: \`${CONFIG.onboardingDescription == null || CONFIG.onboardingDescription == 'null' ? '' : CONFIG.onboardingDescription.replace(/[\\$'"]/g, "\\$&")}\`
  },
  permissionTheme: 'dark'
}`

  fs.writeFileSync('Config.js', fileConfig)
}

function cleanMipmapFolder(dir) { 
  console.log('clean dir mipmap : ' + dir)
  let files = fs.readdirSync(dir);

  files.forEach(file => {
    const exts = file.split('.')
    const ext = exts[exts.length - 1]

    if (ext != 'json') {
      fs.unlinkSync(`${dir}/${file}`)
      console.log(file + ' deleted');
    }
  });
}

async function generateIcons(icon) {
  const exts = icon.split('.')
  const ext = exts[exts.length - 1]

  if (isAndroid) {
    const iconDensity = ['xxxhdpi', 'xxhdpi', 'xhdpi', 'hdpi', 'mdpi']
    const iconSizes = [192, 144, 96, 72, 48]

    for (let i = 0; i < iconDensity.length; i++) {
      const path = `android/app/src/main/res/mipmap-${iconDensity[i]}`
      cleanMipmapFolder(path)

      const result = await sharp(icon)
        .resize(iconSizes[i], iconSizes[i])
        .toFile(`${path}/ic_launcher.${ext}`)

      const result2 = await sharp(icon)
        .resize(iconSizes[i], iconSizes[i])
        .toFile(`${path}/ic_launcher_round.${ext}`)
    }
  } else {
    const iosIconSizes = [1024, 180, 120, 114, 87, 80, 60, 58, 57, 40, 29]
    const path = `ios/WebViewGenerator/Images.xcassets/AppIcon.appiconset`
    cleanMipmapFolder(path)

    for (let i = 0; i < iosIconSizes.length; i++) {
      const result = await sharp(icon)
        .resize(iosIconSizes[i])
        .toFile(`${path}/${iosIconSizes[i]}.${ext}`)
    }
  }
}

function stop() {
  if (isAndroid) {
    const stopProcess = exec("cd android && ./gradlew --stop");

    stopProcess.stdout.on('data', function (data) {
      console.log(data);
    });
  }
}

function moveFileBuild() { 
  let command = ''

  if (isAndroid) {
    command = `mv android/app/build/outputs/${CONFIG.typeBuild == 'apk' ? 'apk' : 'bundle'}/release/${CONFIG.applicationId}-${CONFIG.versionName}\\\(${CONFIG.versionCode}\\\)-${CONFIG.appId}-release.${CONFIG.typeBuild} build/${CONFIG.applicationId}-${CONFIG.versionName}\\\(${CONFIG.versionCode}\\\)-${CONFIG.appId}-release.${CONFIG.typeBuild}`
  } else {
    command = `mv ios/build/WebViewGenerator.ipa build/${CONFIG.applicationId}-${CONFIG.versionName}\\\(${CONFIG.versionCode}\\\)-${CONFIG.appId}-release.${CONFIG.typeBuild}`
  }

  const mvProcess = exec(command);

  mvProcess.on('exit', function (code) {
    if (code != 0) {
      console.log(`failed move ${CONFIG.typeBuild}. exit code = `, code);
    }

    process.exit(code)
  })
}

function downloadKeystore(url) {
  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      const path = './android/app/release.keystore'
      const filePath = fs.createWriteStream(path)
      res.pipe(filePath)

      filePath.on('finish', () => {
        filePath.close()
        resolve('ok')
        console.log('Download keystore completed')
      }).on('error', (err) => { 
        console.log('Download keystore failed')
        fs.unlink(path)
        reject(err.message)
      })
    })
  })
} 

async function execSudo(command) {
  return new Promise((resolve, reject) => {
    sudo.exec(command, function (err, pid, result) {
      if (!err) {
        // moveFileBuild()
        console.log(result);
        resolve(0)
      } else {
        reject(1)
        console.log(`execSudo error  = `, err);
      }
    });
  })
}

async function resignIPA() {
  try {
    console.log('resign IPA..')
    const { stdout, stderr } = await execSync(`security cms -D -i ${iosPath}/distribution.mobileprovision`);
    const json = JSON.parse(parser.toJson(stdout))
    const teamName = json.plist.dict.string[2]
    const teamId = json.plist.dict.array[0].string
    const codeSignIdentity = `Apple Distribution: ${teamName} (${teamId})`

    const resignCommand = `openssl x509 -in ${iosPath}/distribution.cer -inform DER -out ${iosPath}/distribution.pem -outform PEM && ` + 
      `openssl pkcs12 -export -inkey ${iosPath}/distribution.key -in ${iosPath}/distribution.pem -out ${iosPath}/distribution.p12 -passout pass:appsinstant && ` +
      `sudo security authorizationdb write com.apple.trust-settings.admin allow ; sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ${iosPath}/distribution.cer ; sudo security authorizationdb remove com.apple.trust-settings.admin && ` +
      `sudo security authorizationdb write com.apple.trust-settings.admin allow ; sudo security import ${iosPath}/distribution.p12 -k /Library/Keychains/System.keychain -P "123456" ; sudo security authorizationdb remove com.apple.trust-settings.admin && ` +
      `fastlane run resign ipa:"${iosPath}/build/WebViewGenerator.ipa" provisioning_profile:"${iosPath}/distribution.mobileprovision" signing_identity:"${codeSignIdentity}"`
    
    const commands = [
      `openssl x509 -in ${iosPath}/distribution.cer -inform DER -out ${iosPath}/distribution.pem -outform PEM`,
      `openssl pkcs12 -export -inkey ${iosPath}/distribution.key -in ${iosPath}/distribution.pem -out ${iosPath}/distribution.p12 -passout pass:appsinstant`,
      `sudo security authorizationdb write com.apple.trust-settings.admin allow ; sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ${iosPath}/distribution.cer ; sudo security authorizationdb remove com.apple.trust-settings.admin`,
      `sudo security authorizationdb write com.apple.trust-settings.admin allow ; sudo security import ${iosPath}/distribution.p12 -k /Library/Keychains/System.keychain -P "123456" ; sudo security authorizationdb remove com.apple.trust-settings.admin`,
      `fastlane run resign ipa:"${iosPath}/build/WebViewGenerator.ipa" provisioning_profile:"${iosPath}/distribution.mobileprovision" signing_identity:"${codeSignIdentity}"`
    ]
    // const command = `cd ${iosPath}/build && ` +
    //   `rm -rf Payload && ` +
    //   `security cms -D -i ${iosPath}/embedded.mobileprovision > ${iosPath}/provision.plist && ` +
    //   `/usr/libexec/PlistBuddy -x -c 'Print :Entitlements' ${iosPath}/provision.plist > ${iosPath}/entitlements.plist && ` +
    //   `unzip WebViewGenerator.ipa && ` +
    //   `rm -rf Payload/WebViewGenerator.app/_CodeSignature && ` +
    //   `cp ${iosPath}/embedded.mobileprovision Payload/WebViewGenerator.app/embedded.mobileprovision && ` +
    //   `/usr/bin/codesign -f -s "${s}" --entitlements ${iosPath}/entitlements.plist Payload/WebViewGenerator.app && ` +
    //   `/usr/bin/codesign -f -s "${s}" --entitlements ${iosPath}/entitlements.plist Payload/WebViewGenerator.app/Frameworks/* && ` +
    //   `zip -qr "Application.resigned.ipa" Payload`
    
    // const command = `cd ${iosPath}/build && ` +
    //   `rm -rf Payload && ` +
    //   `unzip WebViewGenerator.ipa && ` +
    //   `rm -r "Payload/WebViewGenerator.app/_CodeSignature" "Payload/WebViewGenerator.app/CodeResources" 2> /dev/null | true && ` +
    //   `cp "${iosPath}/embedded.mobileprovision" "Payload/WebViewGenerator.app/embedded.mobileprovision" && ` +
    //   `/usr/bin/codesign -f -s "Apple Distribution: Arif Dedy Kurniawan (Z2K3M84HX5)" "Payload/WebViewGenerator.app" && ` +
    //   `zip -qr "Application.resigned.ipa" Payload`

    // console.log(resignCommand)

    // const resignProcess = exec(resignCommand, function (error, stdout, stderr) {
    //   console.log('stdout', stdout);
    //   console.log('stderr', stderr);
    //   if (error !== null) {
    //     console.log('exec error: ', error);
    //   }
    // });

    for (let i = 0; i < commands.length - 1; i++) {
      const result = await execSudo(commands[i].split(' '))

      if (result === 1) {
        console.log(`resign IPA failed`);
        process.exit(1)
      }
    }
  } catch (error) {
    console.log(error)
    console.log('resign IPA failed')
    process.exit(1)
  }
}

function downloadIosMobilePro(url) {
  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      const path = './ios/distribution.mobileprovision'
      const filePath = fs.createWriteStream(path)
      res.pipe(filePath)

      filePath.on('finish', () => {
        filePath.close()
        resolve('ok')
        console.log('Download mobileprovision completed')
      }).on('error', (err) => { 
        fs.unlink(path)
        reject(err.message)

        console.log(err.message)
        console.log('failed download mobileprovision')
        process.exit(1)
      })
    })
  })
} 

function downloadIosCodesignCsr(url) {
  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      const path = './ios/distribution.csr'
      const filePath = fs.createWriteStream(path)
      res.pipe(filePath)

      filePath.on('finish', () => {
        filePath.close()
        resolve('ok')
        console.log('Download codesign csr completed')
      }).on('error', (err) => { 
        fs.unlink(path)
        reject(err.message)

        console.log(err.message)
        console.log('failed download codesign csr')
        process.exit(1)
      })
    })
  })
} 

function downloadIosCodesignKey(url) {
  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      const path = './ios/distribution.key'
      const filePath = fs.createWriteStream(path)
      res.pipe(filePath)

      filePath.on('finish', () => {
        filePath.close()
        resolve('ok')
        console.log('Download codesign key completed')
      }).on('error', (err) => { 
        fs.unlink(path)
        reject(err.message)

        console.log(err.message)
        console.log('failed download codesign key')
        process.exit(1)
      })
    })
  })
} 

async function main() {
  try {
    console.log('generate ================================================================')
    generateIcons(CONFIG.icon)

    if (isAndroid) {
      console.log('android')
      await downloadKeystore(CONFIG.keystore)
    } else {
      console.log('ios')
      console.log('downloading mobile provision..')
      await downloadIosMobilePro(CONFIG.iosMobilePro)
      console.log('downloading csr..')
      await downloadIosCodesignCsr(CONFIG.iosCodesignCsr)
      console.log('downloading key..')
      await downloadIosCodesignKey(CONFIG.iosCodesignKey)
    }

    initConfig()
    let buildCommand = ''

    if (isAndroid) {
      let file2 = fs.readFileSync('template/strings.xml', 'utf8')
      file2 = file2.replace('{{app_name}}', CONFIG.appName)

      fs.writeFileSync('android/app/src/main/res/values/strings.xml', file2)

      let file3 = fs.readFileSync('template/build.gradle', 'utf8')
      file3 = file3.replace('{{appId}}', CONFIG.appId)
      file3 = file3.replace('{{application_id}}', CONFIG.applicationId)
      file3 = file3.replace('{{versionCode}}', CONFIG.versionCode)
      file3 = file3.replace('{{versionName}}', CONFIG.versionName)
      file3 = file3.replace('{{storePassword}}', md5(CONFIG.applicationId + CONFIG.appId))
      file3 = file3.replace('{{keyAlias}}', CONFIG.applicationId + CONFIG.appId)
      file3 = file3.replace('{{keyPassword}}', md5(CONFIG.applicationId + CONFIG.appId))

      if (CONFIG.fcm === true) {
        file3 = file3.replace("// apply plugin: 'com.google.gms.google-services'", "apply plugin: 'com.google.gms.google-services'")
      }

      fs.writeFileSync('android/app/build.gradle', file3)

      const lastClean = localStorage.getItem('last_clean') ? localStorage.getItem('last_clean') : ''

      buildCommand += 'cd android && '

      const expiredClean = lastClean + ((60 * 1000) * 60) * 24

      if (lastClean == '' || Date.now() > expiredClean) {
        buildCommand += './gradlew clean && '
        await localStorage.setItem('last_clean', Date.now())
      }

      buildCommand += `${CONFIG.typeBuild == 'apk' ? './gradlew app:assembleRelease ' : './gradlew app:bundleRelease '}`
      buildCommand += `--stacktrace`
    } else {
      console.log('bundle identifier id = ' + CONFIG.iosBundleId)

      let file4 = fs.readFileSync('ios/WebViewGenerator.xcodeproj/project.pbxproj', 'utf8')

      file4 = file4.replace(/PRODUCT_BUNDLE_IDENTIFIER = [^"](.*);/g, `PRODUCT_BUNDLE_IDENTIFIER = ${CONFIG.iosBundleId};`)

      fs.writeFileSync('ios/WebViewGenerator.xcodeproj/project.pbxproj', file4)

      let file5 = fs.readFileSync('template/Info.plist', 'utf8')
      file5 = file5.replace('{{appName}}', CONFIG.appName)
      file5 = file5.replace('{{applicationId}}', CONFIG.iosBundleId)
      file5 = file5.replace('{{versionCode}}', CONFIG.versionCode)
      file5 = file5.replace('{{versionName}}', CONFIG.versionName)

      fs.writeFileSync('ios/WebViewGenerator/Info.plist', file5)

      /*

      xcodebuild clean archive -workspace WebViewGenerator.xcworkspace -scheme WebViewGenerator -archivePath WebViewGenerator.xcarchive -allowProvisioningUpdates
      xcodebuild -exportArchive -archivePath WebViewGenerator.xcarchive -exportPath build -exportOptionsPlist ExportOptions.plist
      ./Scripts/xresign.sh -s build/WebViewGenerator.ipa -c "Apple Distribution: Arif Dedy Kurniawan (Z2K3M84HX5)" -p com.appsinstant.appsin.mobileprovision -b com.appsinstant

      */

      buildCommand += `cd ios && `
      buildCommand += `xcodebuild clean archive -workspace ${iosPath}/WebViewGenerator.xcworkspace -scheme WebViewGenerator -archivePath ${iosPath}/WebViewGenerator.xcarchive -allowProvisioningUpdates && `
      buildCommand += `xcodebuild -exportArchive -archivePath ${iosPath}/WebViewGenerator.xcarchive -exportPath ${iosPath}/build -exportOptionsPlist ${iosPath}/ExportOptions.plist `
      // buildCommand += `${iosPath}/Scripts/xresign.sh -s ${iosPath}/build/WebViewGenerator.ipa -c "Apple Distribution: Arif Dedy Kurniawan (Z2K3M84HX5)" -p ${iosPath}/com.appsinstant.appsin.mobileprovision -b com.appsinstant `
    }

    console.log(buildCommand)

    const buildProcess = exec(buildCommand, {
      maxBuffer: 104867500
    });

    buildProcess.stdout.on('data', function (data) {
      console.log(data);
    });


    buildProcess.stderr.on('data', (data) => {
      console.log('======= exec error ======')
      console.log(data.toString());
      console.log('=========================')
    });

    buildProcess.on('exit', function (code) {
      console.log('exit code = ', code)

      if (code != null) {
        if (code == 0) {
          console.log('Build success');
          moveFileBuild()
          // if (isAndroid) {
          //   console.log('Build success');
          //   moveFileBuild()
          // } else {
          //   resignIPA()
          // }
        } else {
          console.log('Build failed');
          process.exit(code)
        }
      }
    })

    const timeOut = setTimeout(() => {
      console.log('TIME OUT');
      stop()
      process.exit(1)
    }, TIME_OUT)
  } catch (err) {
    console.error(err)
    console.log(err)
    stop()
  }
}

main()
