const fs = require('fs')
const { exec } = require("child_process");
const kill = require('tree-kill');
const { io } = require("socket.io-client");
var os = require("os");
var hostname = os.hostname();
var CronJob = require('cron').CronJob;
const { Storage } = require('@google-cloud/storage');
const jwt = require('jsonwebtoken');
const storage = new Storage({
  projectId: 'instantwebtoapp',
  keyFilename: './credentials.json'
});

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  global.localStorage = new LocalStorage('./scratch');
}

let buildIsRunning = false

const data = {
  type: "server_build",
  id: hostname
}

const cert = fs.readFileSync('./config/key/private.key')
const token = jwt.sign({ data: data }, cert, { algorithm: 'RS512' });

const socket = io("https://ws.appsinstant.com", {
  query: { token },
  transports: ['websocket'],
  upgrade: true,
  reconnection: true
});


async function saveImage(name, image) {
  try {
    const imgdata = image;
    const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
    const extension = imgdata.match(/[^:/]\w+(?=;|,)/)[0];
    const path = name + "." + extension
    console.log('path ' + path);
    await fs.writeFileSync(path, base64Data, { encoding: 'base64' });
    return path
  } catch (e) {
    console.log(e)
    return false
  }
}

function replaceGoogleService(credentials) {
  return new Promise((resolve, reject) => {
    fs.writeFile('android/app/google-services.json', credentials, err => {
      if (err) {
        console.log(err)
        reject(err)
        return
      }
    })
    resolve('ok')
  })
}

socket.on("connect", async() => {
  console.log("connect " + socket.id)
  console.log("hostname " + hostname);

  socket.emit("join", {id: socket.id, hostname: hostname})
  
  socket.on("requestCancelBuild", async (data) => {
    const index = queue.findIndex(d => d.id == data.id)
    kill(queue[index].pid)
    socket.emit("requestCancelBuildSuccess", data)
  })

  socket.on("requestBuild", async (data) => {
    data.hostname = hostname
    console.log("requestBuild " + JSON.stringify(data));
    
    if (buildIsRunning) {
      console.log("build is running..");
    } else {
      buildIsRunning = true
      socket.emit("requestBuildProcessed", data)
      const icon = await saveImage('icon', data.icon)
      var splashBg = ''

      if (data.bgImageSplash != '' && data.bgImageSplash != null) {
        splashBg = await saveImage('splash_bg', data.bgImageSplash)
      }

      var onBoardingBg = ''

      if (data.onboarding.bg_image != '' && data.onboarding.bg_image != null) {
        onBoardingBg = await saveImage('on_boarding_bg', data.onboarding.bg_image)
      }

      var splashIcon = ''
      // if (data.bgImageSplashIcon != '' && data.bgImageSplashIcon != null) {
      //   splashIcon = await saveImage('splash_icon', data.bgImageSplashIcon)
      // }

      // firebase_ios, firebase
      var isUseFcm = false
      var fcmCredentials = data.app_credentials.filter(c => c.type == 'firebase' || c.type == 'firebase_ios')
      isUseFcm = (fcmCredentials.length > 0) ? true : false

      if (isUseFcm) {
        await replaceGoogleService(JSON.stringify(fcmCredentials[0].key_value))
      }

      let queue = localStorage.getItem('queue') ? JSON.parse(localStorage.getItem('queue')) : []
      const buildCommand = `node build.js --checkTxtColor "${data.onboarding.check_txt_color}" --bgColorOnBoarding "${data.onboarding.bg_color}" --bgSnapOnBoarding "${data.onboarding.snap_image}" --bgOnBoardingPosition "${data.onboarding.screen_position}" --bgImageOnBoarding "${onBoardingBg}" --btnColor "${data.onboarding.btn_color}" --btnTxtColor "${data.onboarding.btn_txt_color}" --btnTxt "${data.onboarding.btn_txt}" --btnRadius ${data.onboarding.btn_radius} --onboardingDescription "${data.onboarding.onboarding_descriptions}" --appId ${data.app_id} --fcm ${isUseFcm} --keystore "${data.keystore}" --storePassword "${data.applicationId}" --keyAlias "${data.applicationId}" --keyPassword "${data.applicationId}" --typeBuild "${data.typeBuild}" --appName "${data.appName}" --versionCode "${data.versionCode}" --versionName "${data.versionName}" --applicationId "${data.applicationId}" --webUrl "${data.webUrl}" --icon "${icon}" --bgImageSplashIcon "${splashIcon}" --bgImageSplash "${splashBg}" --bgImageSplashPosition "${data.bgImageSplashPosition}" --bgImageSplashSnap "${data.bgImageSplashSnap}" --bgColorSplash "${data.bgColorSplash}" --logoSplashPosition "${data.logoSplashPosition}" --splashScreenTime "${data.splashScreenTime}" --splashScreenOrientation "${data.splashScreenOrientation}" --homeOrientation "${data.homeOrientation}" --iosMobilePro "${data?.ios_config?.file_profile}" --iosBundleId "${data?.ios_config?.identifier}" --iosCodesignCsr "${data?.ios_config?.file_csr}" --iosCodesignKey "${data?.ios_config?.file_csr_key}"`
      console.log(buildCommand)
      const buildProcess = exec(buildCommand, {
        maxBuffer: 104867500
      });
      const index = queue.findIndex(d => d.id == data.id)
      // queue[index].pid = buildProcess.pid
      // await localStorage.setItem('queue', JSON.stringify(queue))

      buildProcess.stdout.on('data', function (out) {
        console.log(out);
      });


      buildProcess.stderr.on('data', (data) => {
        console.log('======= exec error! ======')
        console.log(data.toString());
        console.log('=========================')
      });

      buildProcess.on('exit', async function (code) {
        console.log('exit code = ', code)

        if (code == 0) {
          data.bgImageSplash = ''
          data.icon = ''
          console.log('==== Build success ====');
          console.log(JSON.stringify(data));
          socket.emit("requestBuildSuccess", data)

          
          data.status = 0 // 0 = unuploaded, 1 = uploaded

          if (index > -1) {
            queue[index] = data
          } else {
            queue.push(data)
          }

          await localStorage.setItem('queue', JSON.stringify(queue))
        } else {
          console.log('==== Build failed ====');
          socket.emit("requestBuildFailed", data)
        }

        buildIsRunning = false
      })

      console.log('pid: ', buildProcess.pid)
    }
  })
});

socket.on("disconnect", () => {
  console.log(socket.id)
  socket.off("requestBuild")
  socket.off("requestCancelBuild")
})