const fs = require('fs')
const { exec } = require("child_process");
const { io } = require("socket.io-client");
var os = require("os");
var hostname = os.hostname();
var CronJob = require('cron').CronJob;
const { Storage } = require('@google-cloud/storage');
const jwt = require('jsonwebtoken');
const storage = new Storage({
  projectId: 'instantwebtoapp',
  keyFilename: './credentials.json'
});

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  global.localStorage = new LocalStorage('./scratch');
}

const data = {
  type: "server_build",
  id: hostname
}

const cert = fs.readFileSync('./config/key/private.key')
const token = jwt.sign({ data: data }, cert, { algorithm: 'RS512' });

const socket = io("https://ws.appsinstant.com", {
  query: { token },
  transports: ['websocket'],
  upgrade: true,
  reconnection: true
});

socket.on("connect", () => {
  console.log("connect " + socket.id)
})

function deleteFile(path) {
  fs.unlink(path, (err) => {
    if (err) {
      console.log('failed delete file')
      console.error(err)
    }
  })
}

var job = new CronJob('*/15 * * * * *', async () => {
  var queue = localStorage.getItem('queue') ? JSON.parse(localStorage.getItem('queue')) : []
  var queueAvail = queue.filter(q => q.status === 0)

  for (let i = 0; i < queueAvail.length; i++) {
    const q = queue[i]
    queue[i].status = 1
    await localStorage.setItem('queue', JSON.stringify(queue))

    try {
      const fileName = q.applicationId + '-' + q.versionName + '(' + q.versionCode + ')-' + q.app_id + '-release.' + q.typeBuild
      const filePath = 'build/' + fileName
      const bucketName = 'appsinstant'

      if (!fs.existsSync(filePath)) {
        queue.splice(i, 1)
        await localStorage.setItem('queue', JSON.stringify(queue))
        continue;
      }

      console.log('uploading..')

      const response = await storage.bucket(bucketName).upload(filePath, {
        destination: 'appsinstant' + q.id + '.' + q.typeBuild,
        // public: true
        metadata: {
          cacheControl: 'public, max-age=31536000'
        }
      })

      console.log(response)
      const url = response[0].metadata.mediaLink;
      console.log('url: ' + url)
      console.log('==== Upload success ====');
      socket.emit("requestUploadSuccess", {
        app_id: q.app_id,
        user_id: q.user_id,
        id: q.id,
        url: url,
        typeBuild: q.typeBuild,
        versionName: q.versionName
      })

      queue.splice(i, 1)
      await localStorage.setItem('queue', JSON.stringify(queue))

      deleteFile(filePath)
    } catch (err) {
      console.log('==== Upload failed ====');
      queue[i].status = 0
      await localStorage.setItem('queue', JSON.stringify(queue))

      console.log(err);
    }
  }
});

job.start()

// pm2 start cron_upload.js --exp-backoff-restart-delay=100