const data = {
  WEB_URL: 'https://appon.id',
  BG_COLOR: '#FFFFFF',
  LOGO_SPLASH_POSITION: 'center',
  BG_SPLASH_SNAP: 'center',
  BG_SPLASH_POSITION: 'full_height',
  SPLASH_SCREEN_TIME: 2,
  SPLASH_SCREEN_ORIENTATION: 'portrait',
  HOME_ORIENTATION: 'portrait',
  IS_USE_FCM: false,
  onBoarding: {
    button: {
      bgColor: '#35a0f9',
      textColor: '#FFFFFF',
      text: 'Next',
      borderRadius: 30
    },
    description: 'Loremmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an'
  },
  permissionTheme: 'dark'
} 

const util = require('util');
const fs = require('fs')
const { exec } = require("child_process");
const execSync = util.promisify(require('child_process').exec);
const parser = require('xml2json');

async function resignIPA() {
  try {
    const { stdout, stderr } = await execSync(`security cmss -D -i ios/embedded.mobileprovision`);
    const json = JSON.parse(parser.toJson(stdout))
    const teamName = json.plist.dict.string[2]
    const teamId = json.plist.dict.array[0].string
    const c = `Apple Distribution: ${teamName} (${teamId})`
    console.log(c)
  } catch(error) {

  }
}

function generateKeyStore() {
  const password = '0b0965d68b531419e3301c07e20c10b6'
  const aliasName = 'com.burgerking.app'
  const keystore = 'appsinstant'

  const generateProcess = exec(`printf '${password}\n${password}\n\n\n\n\n\n\ny\n' | keytool -genkey -v -keystore ${keystore}.keystore -alias ${aliasName} -keyalg RSA -keysize 2048 -validity 10000`, function (error, stdout, stderr) {
    console.log('stdout', stdout);
    console.error('stderr', stderr);
    if (error !== null) {
      console.log('exec error: ', error);
    }
  });

  generateProcess.on('exit', function (data) {
    console.log(data);
  });
}

function generateiOSCertificate() {
  const password = 'com.packagename' 
  const email = 'test@mail.com' 

  const generateProcess = exec(`openssl genrsa -out ios-dev.key 2048 && printf '\n\n\n\n\n\n${email}\n${password}\n\n' | openssl req -new -key ios-dev.key -out ios-dev.csr`, function (error, stdout, stderr) {
    console.log('stdout', stdout);
    console.error('stderr', stderr);
    if (error !== null) {
      console.log('exec error: ', error);
    }
  });

  generateProcess.on('exit', function (data) {
    console.log(data);
  });
}


const str = `
				PRODUCT_BUNDLE_IDENTIFIER = com.appsinstant.appsin;
				PRODUCT_NAME = WebViewGenerator;
        
        xxxxxx

				PRODUCT_BUNDLE_IDENTIFIER = com.appsinstant.appsin;
				PRODUCT_NAME = WebViewGenerator;

        xxxxx


				PRODUCT_BUNDLE_IDENTIFIER = "org.reactjs.native.example.$(PRODUCT_NAME:rfc1034identifier)";
				PRODUCT_NAME = "$(TARGET_NAME)";

        `
let file = fs.readFileSync('ios/WebViewGenerator.xcodeproj/project.pbxproj', 'utf8')
const re = str.replace(/PRODUCT_BUNDLE_IDENTIFIER = [^"](.*);/g, 'PRODUCT_BUNDLE_IDENTIFIER = com.test;')

console.log(re)

// resignIPA()